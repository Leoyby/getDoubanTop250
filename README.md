# 豆瓣top250爬虫

#### 介绍
爬取豆瓣top250，结果保存在xls内，并下载剧照

#### 安装教程
本人使用的环境是python3.10.6
需要安装以下的库：
- requests
- parsel
- pandas
- openpyxl

#### 使用说明
xls文件存放在 ./data/  
剧照存放在 ./data/img/
创建相应的文件夹，直接运行即可  
路径可自行修改